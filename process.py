import sqlite3
import numpy as np

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

from sklearn.tree import DecisionTreeClassifier

## Configuration
test_split = 0.30

def loadfromsqlite(filename):
	conn = sqlite3.connect(filename)

	c = conn.cursor()

	# Create table
	c.execute('''SELECT * FROM aggregate''')
	results = c.fetchall()

	# We can also close the connection if we are done with it.
	# Just be sure any changes have been committed or they will be lost.
	conn.close()

	return results

results = loadfromsqlite("output/aggregate_features.sqlite")

X = None
y = np.empty((0))

for row in results:
	ndrow = np.asarray(row[:-1])
	if X is None:
		X = ndrow
	else:
		X = np.vstack((X, ndrow))

	y = np.append(y, row[-1])

### Classify

# Create Multi-Class Labels from Site Names
sites = np.unique(y)

index = 0
labels = []
site_indices = {}

for site in sites:
	labels.append(site)
	site_indices[site] = index

	index += 1

for index in range(len(y)):
	y[index] = site_indices[y[index]]

# Parameters
n_classes = 3
plot_colors = "bry"
plot_step = 0.02


# Shuffle
idx = np.arange(X.shape[0])
# np.random.seed(13)
np.random.shuffle(idx)
X = X[idx]
y = y[idx]
print X.shape
print y.shape
n, d = X.shape

test_number = int(n * test_split)
print test_number

# Train
clf = DecisionTreeClassifier().fit(X[:-test_number], y[:-test_number])

print "Predictions: "

predictions = clf.predict(X[-test_number:])
actual = clf.predict(X[-test_number:])

# Show the results of the classification
print "\n".join([
	"%s ---- %s" % 
	(labels[int(a)], labels[int(b)]) 
	for a, b in zip(predictions, actual)
])
