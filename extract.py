import os
import sys
from subprocess import Popen, PIPE
import md5
import time

# Configuration
outfile = "output.txt"
bro_files = ["ssl_overview.bro", "stream_series.bro"]
bro_flags = "-C"
database_name = "output/aggregate_features"
trace_dir = "data"

overwrite_database = False # Default overwrite db

def system_with_output(command):
  return Popen(
    args=command,
    stdout=PIPE,
    shell=True
  ).communicate()[0][:-1]

def extract_features(files, database):
	for index, file in enumerate(ls):
		path = "../" + trace_dir + "/" + file
		webpage = file.split('-')[0]

		bro_arguments = {
			"SSLOverview::database_name": "../" + database,
			"SSLOverview::webpage_name": webpage,
		}

		bro_argument_str = " ".join(['%s=\\\"%s\\\";' % (key, val) for (key, val) in bro_arguments.iteritems()])

		print "Running Task %d/%d" % (index + 1, len(ls))

		bro_files_up = ["../" + file for file in bro_files]
		bro_command = "cd logs && bro " + bro_flags \
			+ " -r " + path + " " \
			+ " -e \"" + bro_argument_str + "\" " \
			+ " ".join(bro_files_up) \
			+ " > " + outfile
		print bro_command
		
		ouput = system_with_output(bro_command)
		print ouput
	print "[Completed]"

# Overwrite Existing Database
if overwrite_database:
	system_with_output("rm " + database_name + ".sqlite")

# Get Training Files
ls = system_with_output("ls " + trace_dir).split('\n')

extract_features(ls, database_name)
