# Installation

```bash
brew install bro
```

To run local:

```bash
cd logs
```
```bash
bro -C -r ../data/https___en.wikipedia.org_wiki_Barack_Obama-8e482a4aedfd1a5ea10622f565877bb0.pcap
	-e "
		SSLOverview::database_name=\"../output/aggregate_features\"; SSLOverview::webpage_name=\"https___en.wikipedia.org_wiki_Barack_Obama\";
	"
	../ssl_overview.bro ../stream_series.bro > output.txt
```

To run in real-time:

```bash
bro -i en0 ssltest.bro
```

# Features
DONE:
- Extract similar features to the Panchenko paper
- Write these features to SQLite db
- Import from SQLite into NP array and classify using site as label

TODO:
- Implement Bag-Of-Words feature for found domains
- Improve Classifier
- Gather stream features (stream_features.bro)
- Research Stream Classifiers
