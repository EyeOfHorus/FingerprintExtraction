module SSLOverview;

export {
  redef enum Log::ID += { LOG };
  redef enum Log::ID += { AGGREGATE_LOG };

  type Info: record {
    direction: count &log;
    dest_ip: addr &log;
    src_ip: addr &log;
    size: count &log;
  };

  type AggregateFeature: record {
    # Length of bytes outgoing
    request_size: count &log;

    # Packets incoming/outgoing
    incoming_packets: count &log;
    outgoing_packets: count &log;
    incoming_ratio: double &log;

    # Number of Requests
    total_connections: count &log;

    # Flips
    flips: count &log;

    # Size between first and second outgoing packet streams
    estimated_html_length: count &log;
    estimated_html_packets: count &log;

    # The number of different packet lengths in the request
    total_occuring_packet_sizes: count &log;

    # Label
    label: string &log;
  };

  global webpage_name: string;
  global database_name: string;


  global log_packet: event(rec: Info);
  global log_aggregate_features: event(rec: AggregateFeature);
}

global in_ciphers: table[string] of count;
global out_ciphers: table[string] of count;

global ssl_connections: table[conn_id] of bool;
global hostnames: table[conn_id] of string;

global occuring_packet_sizes: set[count] &log;

global aggregate: AggregateFeature;

global outbound: bool;


event bro_init() {
  # Print Arguments
  print "WEBPAGE: ";
  print SSLOverview::webpage_name;
  print "DATABASE FILE: ";
  print SSLOverview::database_name;

  # Initialize Aggregate Features
  aggregate $ request_size = 0;
  aggregate $ incoming_packets = 0;
  aggregate $ outgoing_packets = 0;
  aggregate $ total_connections = 0;
  aggregate $ flips = 0;

  aggregate $ estimated_html_length = 0;
  aggregate $ estimated_html_packets = 0;

  occuring_packet_sizes = set();

  # Create Packets Stream
  Log::create_stream(LOG, [
    $columns=Info,
    $ev=log_packet,
    $path="packets"]
  );

  # Create Aggregate Feature Log Stream
  Log::create_stream(AGGREGATE_LOG, [
    $columns=AggregateFeature, 
    $ev=log_aggregate_features, 
    $path="aggregate"]
  );

  # Use SQLite
  local filter: Log::Filter = [
    $name="sqlite",
    $path=SSLOverview::database_name,
    $config=table(["tablename"] = "aggregate"),
    $writer=Log::WRITER_SQLITE
  ];


  Log::add_filter(SSLOverview::AGGREGATE_LOG, filter);
}

event new_connection_contents(c: connection) {
  print "new connection (not sure what this does)";
}

event ssl_server_hello(c: connection, version: count, possible_ts: time, server_random: string, session_id: string, cipher: count, comp_method: count) {
  local cipher_name = SSL::cipher_desc[cipher];
  
  # Check whether "hello" is incoming or outgoing
  if (Site::is_local_addr( c$id$resp_h )) {

    # Add entry if doesn't exist
    if (cipher_name !in in_ciphers) {
      in_ciphers[cipher_name] = 0;
    }

    # Increment cipher count
    in_ciphers[cipher_name] += 1;
  } else {
    # Add entry if doesn't exist
    if (cipher_name !in out_ciphers) {
      out_ciphers[cipher_name] = 0;
    }

    # Increment cipher count
    out_ciphers[cipher_name] += 1;
  }
}

event ssl_established(c: connection) {
  print "new SSL connection;";

  local id = (c $ id);

  # Add connection ID to table
  if (id !in ssl_connections)
    ssl_connections[id] = T;
}

event tcp_packet (c: connection, is_orig: bool, flags: string, seq: count, ack: count, len: count, payload: string) &priority=10 {
  # Check whether packet belongs to a SSL connection
  local id = (c $ id);
  if (id !in ssl_connections) {
    return;
  }

  # EMPTY packets (ACKs)
  if (len == 0) {
    return;
  }

  # Add to aggregate features
  if (aggregate $ request_size == 0) {
    outbound = is_orig;
  } else if (outbound != is_orig) 
  {
    aggregate $ flips += 1;
    outbound = is_orig;
  }

  if (aggregate $ flips == 2 && !is_orig) {
    aggregate $ estimated_html_packets += 1;
    aggregate $ estimated_html_length += len;
  }

  # Track incoming/outgoing
  if (is_orig) {
    aggregate $ incoming_packets += 1;
  } else {
    aggregate $ outgoing_packets += 1;
  }

  # Track this length as a packet size
  add (occuring_packet_sizes)[len];

  aggregate $ request_size += 1;

  Log::write( SSLOverview::LOG, [
    $direction= |is_orig| ,
    $src_ip=(c $ id $ orig_h),
    $dest_ip=(c $ id $ resp_h),
    $size=(len)
  ]);
}

event ssl_extension_server_name (c: connection, is_orig: bool, names: string_vec) {
  print "ssl_extension_server_name";

  # Associate all the connections with their associated hostnames
  for (name_index in names) {
    hostnames[c$id] = names[name_index];
    print names[name_index];
  }
}

event bro_done() {
  # Log all hostnames
  for (id in hostnames) {
    print hostnames[id];
  }

  local index = 0;

  print "Incoming connections:";
  for ( i in in_ciphers) {
    print fmt("%s was seen in %d connections", i, in_ciphers[i]);
      Log::write( SSLOverview::LOG, [$num=in_ciphers[i],
                                    $factorial_num=index]);
      index += 1;
  }
  print "Outgoing connections:";
  for ( i in out_ciphers)
    print fmt("%s was seen in %d connections", i, out_ciphers[i]);

  print "Total Request Size:";
  print fmt("%d", aggregate $ request_size);

  aggregate $ total_occuring_packet_sizes = |occuring_packet_sizes|;
  aggregate $ total_connections = |ssl_connections|;

  aggregate $ incoming_ratio = ((aggregate $ incoming_packets) * 1.0 ) / aggregate $ outgoing_packets;

  aggregate $ label = SSLOverview::webpage_name;
  Log::write(SSLOverview::AGGREGATE_LOG, aggregate);
}